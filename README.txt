INTRODUCTION
------------

"Editor Sidebar Menu" (esm) adds a configurable sidebar to your form display.
This helps users navigate through long forms with many fields and fieldgroups.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/esm

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/esm

REQUIREMENTS
------------
* Field Group Module has to be installed

Note: This module works best with Seven Theme and comes with default styling which can be overriden. Only desktop and
tablet viewport is supported.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

   It is highly recommended to install "Editor Sidebar Menu" using Composer so its
   dependencies can be installed automatically.

CONFIGURATION
-------------

ESM introduces a new fieldgroup formatter "Area" which allows you to configure sidebar menu items.
You can create a label and add an ID and additional CSS classes. This is useful to add fontawesome-icons
to your sidebar menu entry.



MAINTAINERS
-----------

Current maintainers:
 * Luca Stockmann (lucastockmann) - https://www.drupal.org/u/lucastockmann
 * Lucio Waßill (Cyberschorsch) - https://www.drupal.org/u/cyberschorsch

This project has been sponsored by:
* undpaul
  Drupal experts providing professional Drupal development services.
  Visit https://www.undpaul.de for more information.
